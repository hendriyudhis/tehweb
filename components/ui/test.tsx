<div class="contact-box wow rotateIn animated" style="visibility: visible;">
  <label>
    [text* First-Name placeholder id:contact_form_1_firstname "First Name"]{" "}
  </label>
  <label>
    [text* Last-Name placeholder id:contact_form_1_lastname "Last Name"]{" "}
  </label>
  <p>[text* Job-Title placeholder "Job Title"] </p>
  <p>[text* Company placeholder "Company"] </p>
  <p>[email* Email-Address placeholder "Email Address"] </p>
  <p>[tel* Phone-Number placeholder "Phone Number"] </p>
  <span style="font-size: 16px; color:#888888; font-weight: 500;">
    Fairplay Policy for Accommodation:
  </span>
  <br />
  <span style="font-size: 12px; color:#888888; font-weight: 300;">
    To maintain fairness and efficiency, we have implemented the following
    policies:{" "}
  </span>
  <span style="font-size: 14px; color:#888888; font-weight: 700;">
    Registration Completion:
  </span>{" "}
  <span style="font-size: 12px; color:#888888; font-weight: 300;">
    {" "}
    Please complete your registration to ensure your room stay. Failure to do so
    may result in the revocation of your reservation. Please complete the
    registration process promptly to avoid any inconvenience.
  </span>
  <span style="font-size: 14px; color:#888888; font-weight: 700;">
    Room Allocation:
  </span>{" "}
  <span style="font-size: 12px; color:#888888; font-weight: 300;">
    {" "}
    If we cannot confirm your participation, we may allocate your room to
    another delegate. This ensures that all available accommodations are
    utilized effectively and that as many attendees can benefit from the event.
  </span>
  <span style="font-size: 14px; color:#888888; font-weight: 700;">
    Personal Consumption:
  </span>{" "}
  <span style="font-size: 12px; color:#888888; font-weight: 300;">
    {" "}
    We want to remind you that all in-room dining and minibar expenses are for
    personal consumption. Please be sure to enjoy these amenities responsibly
    and be careful about your personal expenditures during your stay.
  </span>
  <span style="font-size: 12px; color:#888888; font-weight: 300;">
    Adhering to these fairplay policies can create an atmosphere of equality and
    ensure a smooth and enjoyable experience for all participants.
  </span>
  <span style="font-size: 12px; color:#888888; font-weight: 300;">
    Thank you for your understanding and cooperation.
  </span>
  <label style="margin-top: 0px;">
    [checkbox* Terms-Conditions "I accept all the terms and conditions above."]{" "}
  </label>
  <br />
  <div id="contact_form_1_submit">
    <center>[submit "Submit"]</center>
  </div>
</div>;
